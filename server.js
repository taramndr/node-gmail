const 
  express = require('express'),
  Session = require('express-session'),
  {google} = require('googleapis'),
  CircularJSON = require('circular-json'),
  plus = google.plus('v1'),
  OAuth2 = google.auth.OAuth2,
  app = express(),  //starting the express app
  
  port = 5000;

//using session in express
app.use(Session({
    secret: 'pinecode-19890913007',
    resave: true,
    saveUninitialized: true
}));
// set auth as a global default
// google.options({
//   auth: oauth2Client
// });


app.get('/api/customers', (req, res) => {
  const customers = [
    {id: 1, fname: "She", lname: "Hely"},
    {id: 2, fname: "He", lname: "Doe"},
    {id: 3, fname: "They", lname: "Mary"}

  ];

  res.json(customers);
});


function getOAuthClient () {
     return new OAuth2(
        '906555759679-8qjtseuam1tpgq4loht9ndokq84cqaln.apps.googleusercontent.com',
        'i09GBsK8E9uF_RMOhGZ-_5o1',
        'http://localhost:3000/inboxes'
      );
}

function getAuthUrl () {
    var oauth2Client = getOAuthClient();
    // generate a url that asks permissions for Google+ and Gmail
    var scopes = [
      'https://www.googleapis.com/auth/userinfo.profile', 
      'https://www.googleapis.com/auth/userinfo.email', 
      'https://www.googleapis.com/auth/gmail.modify'  
    ];

    var url = oauth2Client.generateAuthUrl({
        access_type: 'offline',
        scope: scopes // If you only need one scope you can pass it as string
    });

    return url;

}

app.get('/oAuthloginUrl', function(req, res){
  var url = getAuthUrl();
  res.send(url);
});

//oauthcallback functions
app.get('/tokens', function(req, res, next){
  var oauth2Client = getOAuthClient();
  var session = req.session;

  var code = '';
  if( req.query.code ) {
    code =  req.query.code; // the query param code
    //saving the token to current session
    session["token"] = code;
  } else if( session["token"] ) {
    code = session["token"];
  }
  
  oauth2Client.getToken(code, function (err, tokens) {   //tokens contains an access_token and an optional refresh_token.

    if (err) {
      console.log(err);
      res.send(err);
      return;
    } 
    
    if (!err) {
    
      oauth2Client.setCredentials(tokens);

      var gmail = google.gmail({auth: oauth2Client, version: 'v1'});
      var userName = '';
      var userEmail = '';

      //Retreive Profile Info of user
      plus.people.get({
          userId: 'me',
          auth: oauth2Client
        }, function (err, response) {
            console.log( "error: " + err );
            userName = response.data.displayName;
            userEmail = response.data.emails[0].value;

            //serialized =  CircularJSON.parse( CircularJSON.stringify(response) ); //JSON.stringify(response);
            //console.log('Name: ' + (err ? err.message : response.data.displayName +  " Email: "+ response.data.emails[0].value ));
        });

        //Retrieve gmail inboxes List
        gmail.users.messages.list({
          userId: 'me',
          maxResults: 10,
          //includeSpamTrash: true,
          //labelIds: ['CATEGORY_UPDATES'],
          //q: "is:unread"  
        },
        function(err, msgResponse) {
          if (err) {
            console.log('The Gmail API returned an error: ' + err);
            return;
          }
          
          var rMsg = msgResponse.data.messages;
          
          if (rMsg.length == 0) {
             console.log('No Msgs found.');
             return;
          } 
          else {
            console.log( "nextPageToken = " + msgResponse.data.nextPageToken );
            console.log( "resultSizeEstimate = " + msgResponse.data.resultSizeEstimate );
             
            var mailInbox = [];
           
            for (var i = 0; i < rMsg.length; i++) {  
              
                  var msgID = rMsg[i].id;
                  //get Detail Msg 
                  gmail.users.messages.get({
                      userId: 'me',
                      id: msgID,
                      //format: 'raw' || 'full'
                  },
                  function(err, msgDetail) {

                    if (err) {
                      console.log('The Gmail API returned an error: ' + err);
                      return;
                    } 

                    //push each msg to array
                    var snippet = msgDetail.data.snippet;
                    var id = msgDetail.data.id;
                    var labelIds = msgDetail.data.labelIds;
                    var headers = msgDetail.data.payload.headers;
                    var subject = getHeader(headers, 'Subject');
                    var date = getHeader(headers, 'Date');
                    var dateFormat = require('dateformat');
                    date = dateFormat(date, "mmm d");

                    var from = getHeader(headers, 'From');

                    mailInbox.push({ snippet: snippet, from: from, subject: subject, date: date });

                    if( mailInbox.length == 10 ){
                      console.log(userName);
                      userData = [];
                      userData.push( { inboxes: mailInbox, uname: userName, uemail: userEmail } );

                      res.json(userData);  
                    }
                   
                  });  
               
              } 
             
          }  

        });
    } 

   
  });

});

function getHeader(headers, name) {
  for (var i = 0; i < headers.length; i++) {
    if (headers[i].name == name ){
      return headers[i].value;
    }
  
  }
}

app.listen(port, () => console.log(`Server started on port ${port}`) );

