import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import App from './App';
import Customers from './components/customers/customers';
import Login from './components/login/login';
import Inboxes from './components/inboxes/inboxes';

import registerServiceWorker from './registerServiceWorker';
import { BrowserRouter as Router, Route, Link } from "react-router-dom";

const loggedIn = 1,
currentLocation = '',
app = document.getElementById('root');

ReactDOM.render(<Router>
					<div className="container">
						<ul className="navList">
					        <li>
					          <Link to="/">Home</Link>
				           </li>
				           <li>
					          <Link to="/login">Login with Google Account</Link>
					        </li>
				           <li>
					          <Link to="/inboxes">Show Mail list</Link>
					        </li>
				     	</ul>
				      	<hr />
						<Route exact path="/" component={App} />
				      	<Route path="/login" component={Login} />
				      	<Route path="/customers" component={Customers} />
				      	<Route exact path="/inboxes" component={Inboxes} />
			      	</div>
				</Router>, app);
registerServiceWorker();
