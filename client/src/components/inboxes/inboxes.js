import React, { Component } from 'react';

class Inboxes extends Component {
  constructor() {
    super();
    this.state = {
        uDatas: []  //Initialize the state
      }
    }

    componentDidMount() {
      //console.log( this.props);
      var tcode = '';
      var fetchUrl = '/tokens?code=';

      if( this.props.location.search ) {
        const code  = this.props.location.search;
        var idx = code.lastIndexOf("code=");
        tcode = code.substring( idx + 5 ).replace("#", "");
        fetchUrl = '/tokens?code='+tcode;
      }

      fetch( '/tokens?code='+tcode, {
          headers : { 
            'Content-Type': 'application/json',
            'Accept': 'application/json'
           }

        })
       .then( res => res.json() )
       .then(uDatas => this.setState({uDatas}, () => console.log('mails Fetched...', uDatas ) ));
    }

    render(){
    
    return (
        <div className="googleMail">
          <h3>User Infos</h3>
           {this.state.uDatas.map((uData, i) => 
             
            <div> 
              Welcome! {uData.uname} - {uData.uemail}
  
              {(typeof(uData.inboxes) == 'object') ?
              <div>
                <h3>Your Mails</h3>
                <table>
                  <tr><td>From</td><td>Subject</td><td>Date</td></tr>

                  {uData.inboxes.map((mail, i) => 
                      <tr>
                      <td>{mail.from}</td>
                      <td>{mail.subject}-<span className="snippet">{mail.snippet} </span></td>
                      <td>{mail.date} </td>

                      </tr>
                  )}
                </table>
                </div> 
                :
                null  

              }
            </div> 
            )}

        </div>
    );
  }
}

export default Inboxes;
