import React, { Component } from 'react';

class UserDetails extends Component {
  constructor() {
  	super();
  	this.state = {
  			details: ''
  		}
  	}

  	componentDidMount() {

      fetch('/udetails', {
          headers : { 
            'Content-Type': 'application/json',
            'Accept': 'application/json'
           }

        })
      .then( res => res.json() )
      .then(details => this.setState({details}, () => console.log('User Details Fetched...', details ) ));
    }

  	render(){
      
    return (
        <div className="userDetail">
              <h3>User Info </h3>
              {this.state.details.name}
              {this.state.details.email}

              
          
        </div>
    );
  }
}

export default UserDetails;
