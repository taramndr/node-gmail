import React, { Component } from 'react';
import './login.css';
import googleIcon from './google.svg';

class Login extends Component {
  constructor() {
  	super();
  	this.state = {
        oAuthurl: null
      }
    }

  	componentDidMount() {

      fetch('/oAuthloginUrl')
      .then((response) => {
        //console.log(response.text);
        return response.text();
      })
      .then((responseData) => {
        this.setState({
          oAuthurl: responseData,
          
        });
      })
  	}

  	render(){
    return (
      <div className="btnHolder">
        <a className="googleBtn" href={this.state.oAuthurl}>
        <img src={googleIcon} className="googleIcon" alt="google"/> Sign in with Google</a>
        
      </div>
    );
  }
}

export default Login;
